**Installation des outils**

- sudo apt-get install heimdall-flash heimdall-flash-frontend (Linux)
- https://odinflashtool.com/downloads/download-heimdall/ (Win-Mac)


**Télécharger : la Rom, la Recovery et Open Gapps**

- https://sourceforge.net/projects/retiredtab/files/SM-P550/TWRP/TWRP_3.5.2_9_SM-P550_20210515_Unofficial.img/download
- https://sourceforge.net/projects/retiredtab/files/SM-P550/18.1/lineage-18.1-20221112-UNOFFICIAL-gt5note10wifi.zip/download
- https://opengapps.org/ (sélectionner architecture Arm, Android 11 et variante Pico)
- Copier la ROM et Gapps sur la carte SD

**Préparer la tablette**

- Paramètres -> Options pour les développeurs -> débug USB (sur tablette)
- Rebooter en mode download : `adb reboot bootloader` (depuis le PC)
- Flasher la recovery : `heimdall flash --no-reboot --RECOVERY recovery.img` (depuis le PC)
- Rebooter la tablette dans la recovery : home/vol-/power dès que l'écran s'éteint basculer de Vol- à Vol+
- Relâcher à l'affichage de "_Set warranty bit : recovery_" en haut à gauche de l'écran


**Sauvegarde ROM stock**

- Sélectionner la langue
- Cliquer sur "Lecture seule"
- cliquer sur Sauvergarder (sic)
- Sélectionner toutes les partitions sauf EFS (Amorçage, Système, Données, Cache)
- Sélectionner l'emplacement (Carte SD)
- Faire glisser le boutton vers la droite


**Installer la nouvelle ROM**

- Cliquer sur retour jusqu'à revenir à l'écran principal.
- (Autoriser les modifications en faisant glisser le boutton)



**Flasher la nouvelle ROM et Open Gapps**

- cliquer sur formater (avancé) tout sélectionner sauf la carte externe et OTG
- Faire glisser le bouton de validation
- cliquer sur retour (2 fois) puis cliquer sur Installer
- Sélectionner le zip contenant la nouvelle ROM
- Cliquer sur "Ajouter d'autre zip"
- Sélectionner Gapps
- Faire glisser le bouton pour lancer l'installation
- formater Cache et Dalvik
- Cliquer sur retour et redémarrer, puis systeme.



**Voilà, plus qu'à lancer la configuration d'Android 11**
